// Package logging provides a partial reimplementation of the log package in the
// standard library. This package provides simple logging with loglevels and an
// interface quite similar to the standard library. There are two environment
// variables supported:
//
// GOLOGGING_LOGLEVEL
//
// In addition to the SetLogLevel() methods, the loglevel can be set globally
// via an environment variable. The recognized values are: ERROR, WARN, INFO,
// DEBUG, and TRACE.
//
// GOLOGGING_MODULES
//
// An optional module string can be set for each logger. This environment
// variable enables filtering on a module basis. A comma seperated list of
// strings is accepted. If GOLOGGING_MODULES is set to e.g. "dial,listen", only
// loggers with the module set to "dial", or "listen" emit output. If you need
// more sophisticated filtering, this can be done outside of your program using
// e.g. syslogd(8) or systemd-journald(8).
//
// For your convenience, there is a package level logger available, which can be
// accessed using the relevant methods. The package level logger is there for
// programs that do not need several loggers. It aims to simplify this
// particular usecase. If you need several loggers, please instantiate them as
// needed and do not use the package level logger.
//
// Timestamps are not supported by design, since this is almost always not
// needed. On the terminal, you can pipe the output through ts(1) from
// moreutils. Using a logging service, the timestamps are already handled
// perfectly. This avoids the annoying problem of doubled timestamps in the log
// database, such as: Aug 10 07:24:37 host service[pid]: TIMESTAMP MESSAGE.
package logging

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
)

const (
	ERROR = iota
	WARN
	INFO
	DEBUG
	TRACE
)

// DefaultLogLevel holds the defalt loglevel of this package.
// It is exported in order to be overwritable it compile time
// by -ldflags="-X ...".
var DefaultLogLevel = WARN

var logModules sort.StringSlice

// Logger is the handle to be used for emitting logmessages.
// Do not instantiate it manually, use NewLogger().
type Logger struct {
	writer   io.Writer
	buf      bytes.Buffer
	logLevel int32
	module   string
	prefix   bool
	mu       sync.Mutex
}

func init() {
	if modules, ok := os.LookupEnv("GOLOGGING_MODULES"); ok {
		mods := strings.Split(modules, ",")
		logModules = sort.StringSlice(mods)
		logModules.Sort()
	}
}

func stringInSlice(slice sort.StringSlice, x string) bool {
	if n := slice.Search(x); n < len(slice) && slice[n] == x {
		return true
	}

	return false
}

// NewLogger creates a fresh instance if Logger. The environment variable
// GOLOGGING_LOGLEVEL is checked whether it is equal to the strings
// ERROR, WARN, INFO, DEBUG, TRACE. In this case, the loglevel of the
// returned Logger is set accordingly. Otherwise, DefaultLogLevel is used.
// Logger by default logs to stderr.
func NewLogger() *Logger {
	logLevel := DefaultLogLevel

	if level, ok := os.LookupEnv("GOLOGGING_LOGLEVEL"); ok {
		switch strings.ToLower(level) {
		case "error":
			logLevel = ERROR
		case "warn":
			logLevel = WARN
		case "info":
			logLevel = INFO
		case "debug":
			logLevel = DEBUG
		case "trace":
			logLevel = TRACE
		default:
			fmt.Printf("logging: unrecognized loglevel: %s\n", level)
		}
	}

	return &Logger{
		writer:   os.Stderr,
		logLevel: int32(logLevel),
		prefix:   true,
	}
}

// SetLogLevel sets the loglevel of the logger. Please use the
// exported constants to set the loglevel accordingly.
func (l *Logger) SetLogLevel(logLevel int) {
	atomic.StoreInt32(&l.logLevel, int32(logLevel))
}

// GetLogLevel retrieves the loglevel from the logger.
func (l *Logger) GetLogLevel() int {
	return int(atomic.LoadInt32(&l.logLevel))
}

// SetModule sets the prefix of the logger. Modules can be
// filtered using the GOLOGGING_MODULES environment variable.
func (l *Logger) SetModule(module string) {
	l.mu.Lock()
	l.module = module
	l.mu.Unlock()
}

// SetWriter sets the output destination for the logger.
func (l *Logger) SetWriter(writer io.Writer) {
	l.mu.Lock()
	l.writer = writer
	l.mu.Unlock()
}

// EnablePrefix enables or disables the logging prefix, for instance "trace: ".
func (l *Logger) EnablePrefix(enabled bool) {
	l.mu.Lock()
	l.prefix = enabled
	l.mu.Unlock()
}

func (l *Logger) output(s string) {
	if len(logModules) > 0 && !stringInSlice(logModules, l.module) {
		return
	}

	if l.module != "" && l.prefix {
		l.buf.WriteString(l.module)
		l.buf.WriteString(": ")
	}

	l.buf.WriteString(s)
	l.buf.WriteString("\n")
	l.buf.WriteTo(l.writer)
}

// Trace logs a message with loglevel trace. A newline is appended.
func (l *Logger) Trace(v ...interface{}) {
	if l.GetLogLevel() >= TRACE {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("trace: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Traceln is the same as Trace.
func (l *Logger) Traceln(v ...interface{}) {
	if l.GetLogLevel() >= TRACE {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("trace: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Tracef logs a message wit loglevel trace. It accepts a Printf style format string.
func (l *Logger) Tracef(format string, v ...interface{}) {
	if l.GetLogLevel() >= TRACE {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("trace: ")
		}
		l.output(fmt.Sprintf(format, v...))
		l.mu.Unlock()
	}
}

// Debug logs a message with loglevel debug. A newline is appended.
func (l *Logger) Debug(v ...interface{}) {
	if l.GetLogLevel() >= DEBUG {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("debug: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Debugln is the same as Debug.
func (l *Logger) Debugln(v ...interface{}) {
	if l.GetLogLevel() >= DEBUG {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("debug: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Debugf logs a message wit loglevel debug. It accepts a Printf style format string.
func (l *Logger) Debugf(format string, v ...interface{}) {
	if l.GetLogLevel() >= DEBUG {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("debug: ")
		}
		l.output(fmt.Sprintf(format, v...))
		l.mu.Unlock()
	}
}

// Info logs a message with loglevel info. A newline is appended.
func (l *Logger) Info(v ...interface{}) {
	if l.GetLogLevel() >= INFO {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("info: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Infoln is the same as Infoln.
func (l *Logger) Infoln(v ...interface{}) {
	if l.GetLogLevel() >= INFO {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("info: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Infof logs a message wit loglevel info. It accepts a Printf style format string.
func (l *Logger) Infof(format string, v ...interface{}) {
	if l.GetLogLevel() >= INFO {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("info: ")
		}
		l.output(fmt.Sprintf(format, v...))
		l.mu.Unlock()
	}
}

// Warn logs a message with loglevel warn. A newline is appended.
func (l *Logger) Warn(v ...interface{}) {
	if l.GetLogLevel() >= WARN {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("warn: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Warnln is the same as Warn.
func (l *Logger) Warnln(v ...interface{}) {
	if l.GetLogLevel() >= WARN {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("warn: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Warnf logs a message wit loglevel warn. It accepts a Printf style format string.
func (l *Logger) Warnf(format string, v ...interface{}) {
	if l.GetLogLevel() >= WARN {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("warn: ")
		}
		l.output(fmt.Sprintf(format, v...))
		l.mu.Unlock()
	}
}

// Error logs a message with loglevel error. A newline is appended.
func (l *Logger) Error(v ...interface{}) {
	if l.GetLogLevel() >= ERROR {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("error: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Errorln is the same as Error.
func (l *Logger) Errorln(v ...interface{}) {
	if l.GetLogLevel() >= ERROR {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("error: ")
		}
		l.output(fmt.Sprint(v...))
		l.mu.Unlock()
	}
}

// Errorf logs a message wit loglevel error. It accepts a Printf style format string.
func (l *Logger) Errorf(format string, v ...interface{}) {
	if l.GetLogLevel() >= ERROR {
		l.mu.Lock()
		l.buf.Reset()
		if l.prefix {
			l.buf.WriteString("error: ")
		}
		l.output(fmt.Sprintf(format, v...))
		l.mu.Unlock()
	}
}

// Fatal logs a message and terminates the program with exit code 1.
func (l *Logger) Fatal(v ...interface{}) {
	l.mu.Lock()
	l.buf.Reset()
	if l.prefix {
		l.buf.WriteString("fatal: ")
	}
	l.output(fmt.Sprint(v...))
	l.mu.Unlock()
	os.Exit(1)
}

// Fatalln is the same as Fatal.
func (l *Logger) Fatalln(v ...interface{}) {
	l.mu.Lock()
	l.buf.Reset()
	if l.prefix {
		l.buf.WriteString("fatal: ")
	}
	l.output(fmt.Sprint(v...))
	l.mu.Unlock()
	os.Exit(1)
}

// Fatalf logs a message and terminates the program with exit code 1.
// It accepts a Printf style format string.
func (l *Logger) Fatalf(format string, v ...interface{}) {
	l.mu.Lock()
	l.buf.Reset()
	if l.prefix {
		l.buf.WriteString("fatal: ")
	}
	l.output(fmt.Sprintf(format, v...))
	l.mu.Unlock()
	os.Exit(1)
}

// Create a package level logger, such that it is possible to do:
// log.Trace("Hi") from outside this package. For the lazy people. :)
var std = NewLogger()

// SetLogLevel is the same as SetLogLevel, but for the package level
// default logger.
func SetLogLevel(logLevel int) {
	std.SetLogLevel(logLevel)
}

// GetLogLevel is the same as GetLogLevel, but for the package level
// default logger.
func GetLogLevel() int {
	return std.GetLogLevel()
}

// SetModule is the same as SetModule, but for the package level
// default logger.
func SetModule(module string) {
	std.SetModule(module)
}

// SetWriter is the same as SetWriter, but for the package level
// default logger.
func SetWriter(writer io.Writer) {
	std.SetWriter(writer)
}

// EnablePrefix is the same as EnablePrefix, but for the package level
// default logger.
func EnablePrefix(enabled bool) {
	std.EnablePrefix(enabled)
}

// Trace is the same as Trace, but for the package level
// default logger.
func Trace(v ...interface{}) {
	std.Trace(v...)
}

// Traceln is the same as Traceln, but for the package level
// default logger.
func Traceln(v ...interface{}) {
	std.Traceln(v...)
}

// Tracef is the same as Tracef, but for the package level
// default logger.
func Tracef(format string, v ...interface{}) {
	std.Tracef(format, v...)
}

// Debug is the same as Debug, but for the package level
// default logger.
func Debug(v ...interface{}) {
	std.Debug(v...)
}

// Debugln is the same as Debugln, but for the package level
// default logger.
func Debugln(v ...interface{}) {
	std.Debugln(v...)
}

// Debugf is the same as Debugf, but for the package level
// default logger.
func Debugf(format string, v ...interface{}) {
	std.Debugf(format, v...)
}

// Info is the same as Info, but for the package level
// default logger.
func Info(v ...interface{}) {
	std.Info(v...)
}

// Infoln is the same as Infoln, but for the package level
// default logger.
func Infoln(v ...interface{}) {
	std.Infoln(v...)
}

// Infof is the same as Infof, but for the package level
// default logger.
func Infof(format string, v ...interface{}) {
	std.Infof(format, v...)
}

// Warn is the same as Warn, but for the package level
// default logger.
func Warn(v ...interface{}) {
	std.Warn(v...)
}

// Warnln is the same as Warnln, but for the package level
// default logger.
func Warnln(v ...interface{}) {
	std.Warnln(v...)
}

// Warnf is the same as Warnf, but for the package level
// default logger.
func Warnf(format string, v ...interface{}) {
	std.Warnf(format, v...)
}

// Error is the same as Error, but for the package level
// default logger.
func Error(v ...interface{}) {
	std.Error(v...)
}

// Errorln is the same as Errorln, but for the package level
// default logger.
func Errorln(v ...interface{}) {
	std.Errorln(v...)
}

// Errorf is the same as Errorf, but for the package level
// default logger.
func Errorf(format string, v ...interface{}) {
	std.Errorf(format, v...)
}

// Fatal is the same as Fatal, but for the package level
// default logger.
func Fatal(v ...interface{}) {
	std.Fatal(v...)
}

// Fatalln is the same as Fatalln, but for the package level
// default logger.
func Fatalln(v ...interface{}) {
	std.Fatalln(v...)
}

// Fatalf is the same as Fatalf, but for the package level
// default logger.
func Fatalf(format string, v ...interface{}) {
	std.Fatalf(format, v...)
}
