# logging

[![GoDoc](https://godoc.org/gitlab.com/rumpelsepp/logging?status.svg)](https://godoc.org/gitlab.com/rumpelsepp/logging)

Package logging provides a partial reimplementation of the log package in the
standard library. This package provides simple logging with loglevels and an
interface quite similar to the standard library.

## Quickstart

```go
package main

import log "gitlab.com/rumpelsepp/logging"

func main() {
    log.SetLogLevel(log.TRACE)
    log.Traceln("trace message")
    log.Debugln("debug message")
    log.Infoln("info message")
    log.Warnln("warn message")
    log.Errorln("error message")
    log.Fatalln("fatal message")
}
```
